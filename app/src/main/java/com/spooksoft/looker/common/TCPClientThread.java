package com.spooksoft.looker.common;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.PasswordAuthentication;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Spook on 2017-05-18.
 */

public class TCPClientThread extends HandlerThread implements INetClientThread {

    private static final String THREAD_NAME = "TCPClient";

    private static final int WM_SEND_STRING = 1;
    private static final int WM_SEND_BYTES = 2;

    private Handler handler = null;
    private Socket socket = null;
    private BufferedWriter outputStream = null;
    private boolean isConnected = false;
    private final String address;
    private final int port;
    private final ConnectionResultListener connectionResultListener;

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();

        try {
            InetAddress server = InetAddress.getByName(address);
            socket = new Socket(server, port);
            outputStream = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        } catch (Exception e) {

            if (socket != null && socket.isConnected()) {
                try {
                    socket.close();
                } catch (Exception e1) {

                }
            }
            socket = null;
            handler = null;
            outputStream = null;

            isConnected = false;
            if (connectionResultListener != null)
                connectionResultListener.failure(e.getMessage());

            return;
        }

        handler = new Handler(getLooper()) {

            @Override
            public void handleMessage(Message msg) {

                if (msg.what == WM_SEND_STRING && msg.obj instanceof String) {
                    String message = (String)msg.obj;
                    try {
                        outputStream.write(message, 0, message.length());
                        outputStream.flush();
                    } catch (IOException e) {
                        Log.e("TCP client thread", "Cannot send message!");
                    }
                } else if (msg.what == WM_SEND_BYTES && msg.obj instanceof char[]) {

                    char[] data = (char[])msg.obj;
                    try {
                        outputStream.write(data, 0, data.length);
                    } catch (IOException e) {
                        Log.e("TCP client thread", "Cannot send message!");
                    }
                }
            }
        };

        isConnected = true;
        connectionResultListener.success();
    }

    public TCPClientThread(String address, int port, ConnectionResultListener connectionResultListener) {
        super(THREAD_NAME);

        this.address = address;
        this.port = port;
        this.connectionResultListener = connectionResultListener;
    }

    @Override
    public void send(String message) {

        if (!isConnected)
            throw new RuntimeException("Cannot send data, not connected!");

        Message msg = new Message();
        msg.obj = message;
        msg.what = WM_SEND_STRING;
        handler.sendMessage(msg);
    }

    public void send(char[] data) {

        if (!isConnected)
            throw new RuntimeException("Cannot send data, not connected!");

        Message msg = new Message();
        msg.obj = data;
        msg.what = WM_SEND_BYTES;
        handler.sendMessage(msg);
    }

    @Override
    public void close() {

        if (!isConnected)
            throw new RuntimeException("Cannot close, not connected!");

        try {
            socket.close();
        } catch (IOException e) {
            Log.e("Looker", "Cannot close socket!");
        }
    }
}
