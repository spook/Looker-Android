package com.spooksoft.looker.services;

import com.spooksoft.looker.common.ConnectionResultListener;
import com.spooksoft.looker.common.INetClientThread;
import com.spooksoft.looker.common.TCPClientThread;
import com.spooksoft.looker.common.UDPClientThread;

/**
 * Created by wsura on 19.05.2017.
 */

public class NetClientService {

    public interface ConnectionListener {
        void success();
        void failure(String reason);
    }

    private enum State {
        Disconnected,
        Connecting,
        Connected
    }

    public enum ConnectionKind {
        TCP,
        UDP
    }

    private INetClientThread thread = null;
    private State state;
    private ConnectionListener connectionListener;

    public void connect(String address, int port, ConnectionKind kind, final ConnectionListener connectionListener) {

        if (state == State.Connected || state == State.Connecting)
            throw new RuntimeException("Already connected!");

        ConnectionResultListener connectionResultListener = new ConnectionResultListener() {
            @Override
            public void success() {
                state = State.Connected;
                connectionListener.success();
            }

            @Override
            public void failure(String reason) {
                state = State.Disconnected;
                connectionListener.failure(reason);
            }
        };

        switch (kind) {
            case TCP:
                thread = new TCPClientThread(address, port, connectionResultListener);
                break;
            case UDP:
                thread = new UDPClientThread(address, port, connectionResultListener);
        }

        thread.start();
    }

    public void disconnect() {

        if (state != State.Connected)
            throw new RuntimeException("Not connected!");

        thread.close();
        thread.quit();
        thread = null;

        state = State.Disconnected;
    }

    public void send(String data) {

        if (state != State.Connected)
            throw new RuntimeException("Not connected!");

        thread.send(data);
    }

    public void send(char[] data) {

        if (state != State.Connected)
            throw new RuntimeException("Not connected!");

        thread.send(data);
    }
}
