package com.spooksoft.looker.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.spooksoft.looker.App;
import com.spooksoft.looker.R;
import com.spooksoft.looker.databinding.ActivityMainBinding;
import com.spooksoft.looker.viewmodels.MainActivityViewModel;
import com.spooksoft.looker.viewmodels.interfaces.IMainActivityAccess;

public class MainActivity extends AppCompatActivity implements IMainActivityAccess {

    // Private fields ---------------------------------------------------------

    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;

    // Private methods --------------------------------------------------------

    private void initializeControls() {

        setSupportActionBar(binding.mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // Protected methods ------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = new MainActivityViewModel(this, this, App.getNewSensorService(), App.getMessagingService());
        binding.setViewModel(viewModel);

        initializeControls();
    }
}